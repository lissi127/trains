import glob
import pygame
from map import *

class App:
    def __init__(self):
        self.size = self.width, self.height = glob.width, glob.height
        self.screen = pygame.display.set_mode(self.size, pygame.HWSURFACE | pygame.DOUBLEBUF) 
        self.screen.fill(glob.bgcolor)

        self._running = True

        self.map = Map(fields=glob.mapka)
        """
        self.render = render()
        self.loop = loop()
        """

    def on_event(self, event):
        if event.type == pygame.QUIT:
            self._running = False
        elif event.type == pygame.MOUSEBUTTONDOWN:
            mouse_pos = pygame.mouse.get_pos()
            print("mouse clicked")
            """
            self.loop.on_click(self.screen, mouse_pos)
            """

        elif event.type == pygame.KEYDOWN:
            print("key down") 

    def on_loop(self):
        pass
    def on_render(self):
        self.screen.fill(glob.bgcolor)
        self.map.draw(self.screen)
        """
            self.render.draw_bracelet(self.bracelet, self.screen)
            self.render.draw_mrizka(self.mrizka,self.bracelet, self.screen)
            self.render.draw_colors(self.color_row,self.screen)
            self.render.draw_buttons(self.buttons,self.screen)
        """
        pygame.display.flip()
    def on_execute(self):
        while self._running:
            for event in pygame.event.get():
                self.on_event(event)
            self.on_loop()
            self.on_render()


creative = App()
creative.on_execute()
