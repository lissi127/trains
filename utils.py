import glob
import pygame, random, time, copy
import numpy as np

def to_coordinates(x, y, frac):
    x = x*frac + frac/2
    y = y*frac + frac/2
    return (x, y)

def draw_image(img_name, x, y, screen, frac):
    img = pygame.image.load("img/"+ str(img_name) + ".png")
    img = pygame.transform.scale(img, (frac, frac))
    img_rect = img.get_rect(center=(x, y))
    screen.blit(img, img_rect)

class Cross:
    def __init__(self, states, x, y):
        self.states = states
        idx = random.randint(0, len(states)-1)
        self.idx = idx
        self.act_state = states[idx]
        self.x = x
        self.y = y
    
    def draw(self, screen, frac):
        (x, y) = to_coordinates(self.x, self.y, frac)
        draw_image(self.act_state, x, y, screen, frac)

    def change_state(self):
        self.idx = (self.idx+1)%len(self.states)
        self.act_state = self.states[self.idx]

class Train:
    def __init__(self, color, pos, direct, v=10):
        self.color = color
        self.direct = direct
        self.pos = pos
        self.v = v

    def move(self, mapka):
        field = mapka.get_field(self.pos)
        center = mapka.get_center(self.pos)
        if isinstance(field, Cross):
            field = field.act_state
        
        change = False
        for i in range(2):
            act = self.pos[i]
            future = act + self.v*self.direct[i]
            if np.sign(center[i]-act) != np.sign(center[i]-future):
                change = True

        if change and field in glob.bends:
            self.compute_direct(field, center)
        elif change and isinstance(field, Depo):
            field.control_train(train=self, mapka=mapka)
        else:
            self.pos[0] += self.v*self.direct[0]
            self.pos[1] += self.v*self.direct[1]

    def compute_direct(self, field, center):
        to_center = [0, 0]
        for i in range(2):
            to_center[i] = center[i] - self.pos[i]
        signed_direct = tuple(np.sign(self.direct))
        direct = glob.to_directs[signed_direct]
        field = field.replace(direct, "")
        self.direct = glob.directs[field]
            
        for i in range(2):
            self.pos[i] += to_center[i] + (self.v - abs(to_center[i]))*self.direct[i]

    def draw(self, screen, frac):
        draw_image("T" + str(self.color), self.pos[0], self.pos[1], screen, frac//2)

class Trains:
    def __init__(self, s_pos, mapka, direct=(-1, 0), trains=[]):
        self.trains = trains
        self.s_pos = s_pos
        self.direct = direct
        self.last_gen = time.time()
        self.gen_lim = 0
        self.last_move = time.time()
        self.mapka = mapka

    def add_train(self, train):
        self.trains.append(train)

    def del_train(self):
        first = self.trains.pop(0)
        return first

    def move(self):
        now = time.time()
        if abs(now-self.last_move) > 0.001:
            for t in self.trains:
                t.move(self.mapka)

    def draw(self, screen, frac):
        self.move()
        for t in self.trains:
            t.draw(screen, frac)

    def generate(self):
        now = time.time()
        if abs(now-self.last_gen) > self.gen_lim:
            self.last_gen = now
            self.gen_lim = random.randint(5, 12)
            idx = random.randint(0, len(glob.colors)-1)
            color = glob.colors[idx]
            train = Train(color, copy.copy(self.s_pos), self.direct) #s_pos should be static variable, therefore I don't wanna pass it as reference
            self.add_train(train)
            for train in self.trains:
                print("vlacek:", train.color, train.pos)

class Depo:
    def __init__(self, color, x, y):
        self.color = color
        self.x = x
        self.y = y
        self.points = 0

    def draw(self, screen, frac):
        (x, y) = to_coordinates(self.x, self.y, frac)
        draw_image("D"+ self.color, x, y, screen, frac)

    def control_train(self, train, mapka):
        mapka.trains.del_train()
        if train.color == self.color:
            print("JUPIIIIII! MAS +1 BOD")
        else:
            print("FNUK. TOHLE BYLO SPATNE DEPO")
        print("MAS", self.points, "BODU!")
