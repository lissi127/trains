import glob, utils
import pygame, random, time
import numpy as np

class Map:
    def __init__(self, fields = []):
        self.fields = fields
        self.x = len(fields[0])
        self.y = len(fields)

        xfrac = glob.width/self.x
        yfrac = glob.height/self.y
        self.frac = int(min(xfrac, yfrac)) #count size of one square

        n_colors = sum(x.count('D') for x in self.fields)
        print(n_colors)
        perm = np.random.permutation(n_colors)
        perm_idx = 0

        for i in range(self.y):
            for j in range(self.x):
                splitted = fields[i][j].split('-')
                if len(splitted) > 1:
                    fields[i][j] = utils.Cross(splitted, j, i)
                elif splitted[0] == "D":
                    color = glob.colors[perm[perm_idx]]
                    perm_idx += 1
                    depo = utils.Depo(color, j, i)
                    self.fields[i][j] = depo
                elif splitted[0] == "S":
                    s_pos = [int(j*self.frac + self.frac/2), int(i*self.frac + self.frac/2)]

        self.trains = utils.Trains(s_pos, mapka=self)

    def draw(self, screen):
        self.trains.generate()
        for i in range(self.y):
            for j in range(self.x):
                pygame.draw.rect(screen, glob.black, (j*self.frac, i*self.frac, self.frac, self.frac), 3)
                field = self.fields[i][j]
                (x, y) = utils.to_coordinates(j, i, self.frac)
                if not isinstance(field, str):
                    field.draw(screen, self.frac)
                elif field == "*": #the field is grass
                    utils.draw_image("G", x, y, screen, self.frac)
                elif field == "S": #the field is grass
                    utils.draw_image("S", x, y, screen, self.frac)
                elif isinstance(field, utils.Depo):
                    field.draw(screen, self.frac)
                else:
                    utils.draw_image(field + "2", x, y, screen, self.frac)
        self.trains.draw(screen, self.frac)
    
    def get_field(self, pos):
        i = int(pos[0]/self.frac)
        j = int(pos[1]/self.frac)
        field = self.fields[j][i]
        return field

    def get_center(self, pos):
        i = int(pos[0]/self.frac)*self.frac + self.frac/2
        j = int(pos[1]/self.frac)*self.frac + self.frac/2
        return (i, j)

