import pygame 

bgcolor = (255, 255, 255)
black = (0, 0, 0)
red = (255, 0, 0)
width = 1000
height = 800

#possible map layouts: * = grass, D = depo, S = start, LU, LD, RU, RD, H, V
mapka = [["*", "*", "D", "*", "D", "*", "*"], \
        ["D", "D", "LD-V", "D", "LD-V", "*", "D"], \
        ["RD-V", "D", "V", "*", "V", "D", "LD-V"], \
        ["RU", "H", "LD-V", "RD", "LU-H", "H", "LU"], \
        ["*", "*", "RU", "RU-H", "H", "H", "S"], \
        ]

bends = ("LU", "LD", "RU", "RD")
directs = {"L": (-1, 0), "R": (1, 0), "U": (0, -1), "D": (0, 1)}
to_directs = {(-1, 0): "R", (1, 0): "L", (0, -1): "D", (0, 1): "U"}

colors = ["acryl", "black", "blue", "brown", "orange", "pink", "red", "yellow"]
